<?php
// 删除一个月以前的日志
foreach ( list_file ( LOG_PATH ) as $f ) {
	if ($f ['isDir']) {
		foreach ( list_file ( $f ['pathname'] . '/', '*.log' ) as $ff ) {
			if ($ff ['isFile']) {
				if (time () - $ff ['mtime'] > 86400 * 30) {
					@unlink ( $ff ['pathname'] );
				}
			}
		}
	}
}
// 删除一天以前的临时文件
foreach ( list_file ( TEMP_PATH ) as $f ) {
	if ($f ['isFile'] && time () - $f ['mtime'] > 86400 && $f ['filename'] != C ( 'DIR_SECURE_FILENAME' )) {
		@unlink ( $f ['pathname'] );
	}
}