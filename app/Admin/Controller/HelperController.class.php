<?php

namespace Admin\Controller;

use \Think\Controller;

class HelperController extends Controller
{
    public function packsrc()
    {
        $this->_packsrc_recursion('./asserts/');
        echo 'OK';
    }

    private function _packsrc_recursion($dir)
    {
        $ignore_file = array('jquery-2.0.0.src.js');
        foreach (list_file($dir) as $f) {
            if ($f ['isDir']) {
                $this->_packsrc_recursion($f ['pathname'] . '/');
            } else if ($f ['isFile']) {
                if (substr($f ['filename'], -8) == '.src.css') {
                    $pack_path = substr($f ['pathname'], 0, -8) . '.css';
                    if (!file_exists($pack_path) || filemtime($pack_path) < filemtime($f ['pathname'])) {
                        $this->_pack_css($f ['pathname'], $pack_path);
                        echo $pack_path . "\n";
                    }
                } else if (substr($f ['filename'], -7) == '.src.js') {
                    $pack_path = substr($f ['pathname'], 0, -7) . '.js';
                    if (!file_exists($pack_path) || filemtime($pack_path) < filemtime($f ['pathname'])) {
                        if (in_array($f['filename'], $ignore_file)) {
                            @copy($f ['pathname'], $pack_path);
                        } else {
                            $this->_pack_js($f ['pathname'], $pack_path);
                        }
                        echo $pack_path . "\n";
                    }
                }
            }
        }
    }

    private function  _pack_css($srcpath, $descpath)
    {
        $content = file_get_contents($srcpath);
        $content = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*' . '/!', '', $content);
        $content = str_replace(array(
            "\r\n",
            "\r",
            "\n",
            "\t",
            '  ',
            '    ',
            '    '
        ), '', $content);
        $content = str_replace(array(
            " 0px",
            " 0em",
            ' 0pt'
        ), ' 0', $content);
        $content = preg_replace('/ *; *' . '/', ';', $content);
        $content = preg_replace('/ *: *' . '/', ':', $content);
        $content = preg_replace('/ *\\} *' . '/', '}', $content);
        $content = preg_replace('/ *\\{ *' . '/', '{', $content);
        $content = preg_replace('/; *\\}/', '}', $content);
        file_put_contents($descpath, $content);
    }

    private function _pack_js($src, $des)
    {
        $url = 'http://tool.lu/js/ajax.html';
        import('Common.Extends.Snoopy');
        $snoopy = new \Snoopy();
        $snoopy->agent = "(compatible; MSIE 4.01; MSN 2.5; AOL 4.0; Windows 98)";
        $snoopy->referer = $url;

        $post = array();
        $post['operate'] = 'uglify';
        $post['code'] = file_get_contents($src);
        $t1 = microtime(true);

        if ($snoopy->submit($url, $post)) {
            $json = @json_decode($snoopy->getResults(), true);
            if (!empty($json['text'])) {
                $t2 = microtime(true);
                file_put_contents($des, $json['text']);
            }
        } else {
            $t2 = microtime(true);
        }

        $time = sprintf('%.4f', ($t2 - $t1));
        return $src . " -&gt; " . $des . " cost " . $time . ' s';
    }
}