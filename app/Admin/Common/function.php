<?php
use Org\Util\Rbac;

/**
 * 递归重组节点信息为多维数组
 *
 * @param array $node        	
 * @param number $pid        	
 */
function node_merge(&$node, $pid = 0) {
	$arr = array ();
	
	foreach ( $node as $v ) {
		if ($v ['pid'] == $pid) {
			$v ['_child'] = node_merge ( $node, $v ['id'] );
			$arr [] = $v;
		}
	}
	
	return $arr;
}
/**
 * 检测当前访问是否有权限
 *
 * @param string $action        	
 * @param string $controller        	
 * @param string $module        	
 * @param number $level        	
 * @return boolean
 */
function access_permit($action = null, $controller = null, $module = null, $level = 3) {
	static $access_list = null;
	if (null === $access_list) {
		C ( 'RBAC_ROLE_TABLE', C ( 'DB_PREFIX' ) . 'admin_role' );
		C ( 'RBAC_USER_TABLE', C ( 'DB_PREFIX' ) . 'admin_role_user' );
		C ( 'RBAC_ACCESS_TABLE', C ( 'DB_PREFIX' ) . 'admin_access' );
		C ( 'RBAC_NODE_TABLE', C ( 'DB_PREFIX' ) . 'admin_node' );
		$access_list = Rbac::getAccessList ( ADMIN_UID );
	}
	if (ADMIN_UID == intval ( C ( 'ADMIN_ROOT_ID' ) )) {
		return true;
	}
	if (null == $module) {
		$module = MODULE_NAME;
	}
	if (null == $controller) {
		$controller = CONTROLLER_NAME;
	}
	if (null == $action) {
		$action = ACTION_NAME;
	}
	switch ($level) {
		case 1 :
			return ! empty ( $access_list [strtoupper ( $module )] );
		case 2 :
			return ! empty ( $access_list [strtoupper ( $module )] [strtoupper ( $controller )] );
		case 3 :
			return ! empty ( $access_list [strtoupper ( $module )] [strtoupper ( $controller )] [strtoupper ( $action )] );
	}
}
/**
 * 当前用户是否为网站创建者
 */
function is_founder() {
	return (ADMIN_UID == intval ( C ( 'ADMIN_ROOT_ID' ) ));
}
/**
 * 获取Controller名称
 *
 * @param string $type        	
 */
function get_admin_controller($type = 'cms|mod|exportable') {
	static $db_tables = null;
	static $db_prefix = null;
	if (null == $db_tables) {
		$db_tables = \Think\Db::getInstance ()->getTables ();
		$db_prefix = C ( 'DB_PREFIX' );
	}
	$controllers = array ();
	if (in_array ( $type, array (
			'cms',
			'mod',
			'exportable' 
	) )) {
		foreach ( list_file ( 'app/Admin/Controller/', '*Controller.class.php' ) as $controler ) {
			$controller_name = substr ( $controler ['filename'], 0, - 20 );
			if (! in_array ( $controller_name, array (
					'Admin',
					'Install',
					'Login',
					'Publish',
					'Cms',
					'Mod' 
			) )) {
				$class = "\\Admin\\Controller\\${controller_name}Controller";
				$ins = new $class ();
				
				switch ($type) {
					case 'exportable' :
						$controllers [] = array (
								'name' => $controller_name 
						);
						break;
					case 'cms' :
						if (strpos ( get_parent_class ( $ins ), ucwords ( $type ) . 'Controller' ) !== false) {
							
							$total_count = '[TABLE NOT EXISTS]';
							if (in_array ( $db_prefix . $ins->cms_table, $db_tables )) {
								$one = M ( $ins->cms_table )->field ( 'COUNT(*) as total' )->find ();
								$total_count = $one ['total'];
							}
							$controllers [] = array (
									'name' => substr ( $controller_name, strlen ( $type ) ),
									'table' => $ins->cms_table,
									'db_engine' => $ins->cms_db_engine,
									'fields_cnt' => count ( $ins->cms_fields ),
									'record_cnt' => $total_count 
							);
						}
						break;
					case 'mod' :
						if (strpos ( get_parent_class ( $ins ), ucwords ( $type ) . 'Controller' ) !== false) {
							$controllers [] = array (
									'name' => substr ( $controller_name, strlen ( $type ) ) 
							);
						}
						break;
				}
			}
		}
	}
	return $controllers;
}